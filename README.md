# honeypot-bot

## Requirements install

```shell
pip install -r requirements.txt
```

## Run bot script

```shell
python3 main.py
```

> Bot logs are placed into the `ShifuTheMasterBot.log` file