import logging
import re
import requests

from decouple import config
from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ParseMode

BOT_TOKEN = config('BOT_TOKEN')

HONEYPOT_API_URL = config('HONEYPOT_API_URL')
QUERY_HEADERS = {
    "X-RapidAPI-Key": config('X_RAPID_API_KEY'),
    "X-RapidAPI-Host": config('X_RAPID_API_HOST')
}

EXCHANGE_NAME = config('EXCHANGE_NAME')
CHAIN_NAME = config('CHAIN_NAME')
ADDRESS_REGEX = '^0x[0-9a-zA-Z]{40}$'

logging.basicConfig(level=logging.INFO, filename='ShifuTheMasterBot.log')
bot = Bot(token=BOT_TOKEN)
dp = Dispatcher(bot)


def send_request(session, params, endpoint):
    check = session.get(url=HONEYPOT_API_URL + endpoint, params=params, headers=QUERY_HEADERS).json()
    logging.info(check)
    return check


def get_exchange_addresses(session):
    return {'factory_address': '0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f',
            'router_address': '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'}


def get_token_USDC_pairs(session):
    return '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'


def check_honeypot(session, token_a):
    exchange = get_exchange_addresses(session)
    USDC_pair = get_token_USDC_pairs(session)
    query_params = {
        "factory_address": exchange['factory_address'],
        "token_b": USDC_pair,
        "chain": CHAIN_NAME,
        "exchange": EXCHANGE_NAME,
        "token_a": token_a,
        "router_address": exchange['router_address']
    }

    check = send_request(session, query_params, 'scan/')
    return prepare_check_answer(check)


def prepare_check_answer(check):
    error = check.get('error', None)
    print(check)
    if error in ['PAIR_NOT_FOUND', 'TOKEN_A_NOT_FOUND', 'TOKEN_B_NOT_FOUND']:
        return f"<b>❌ Found no liquidity pairs of this token. ❌</b>"

    error_string = ''
    message_string = ''

    if error is not None:
        error_string = f"\n<b>Check errors:</B> {error}"

    if check.get('message', None) is not None:
        message_string = f"\n<b>Error message:</B> {check['message']}"

        return f"<b>✅ This contract address is not a honeypot ✅</b>" \
               f"\n<b>Token Name:</b> {check['name']}" \
               f"\n<i>{check['address']}</i>" \
               f"\n<b>Total Supply:</b> {check['total_supply']}" \
               f"\n<b>Buy Tax:</b> {check['buy_tax']}%  |  <b>Sell Tax:</b> {check['sell_tax']}%" \
               f"\n<b>Buy Gas:</b> {check['buy_gas']}  |  <b>Sell Gas:</b> {check['sell_gas']}" \
               f"\n<b>Is honeypot:</B> {check['is_honeypot']}" + error_string + message_string


@dp.message_handler(commands=['start'])
async def start(message: types.Message):
    await message.answer(f"<b>To check token send a contract address starting with 0x...</b>")


@dp.message_handler()
async def inline_check_address(message: types.Message):
    session = requests.Session()
    address = message.text
    is_address_string_valid = not bool(re.search(ADDRESS_REGEX, message.text))
    if is_address_string_valid:
        await message.answer('Please provide valid token address')
        return

    result = check_honeypot(session, address)
    session.close()
    await message.answer(text=result, parse_mode=ParseMode.HTML, disable_notification=True)


executor.start_polling(dp)
